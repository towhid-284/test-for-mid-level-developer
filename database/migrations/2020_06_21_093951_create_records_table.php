<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('from_statement')->default(1)->comment('true means from signal statement');
            $table->string('financial_instrument_code', 8)->nullable()->comment('gbpeur')->collation('utf8mb4_unicode_ci');
            $table->enum('action', ['buy', 'sell'])->collation('utf8mb4_unicode_ci')->default('buy');
            $table->decimal('entry_price', 19, 8)->nullable();
            $table->decimal('closed_price', 19, 8)->nullable();
            $table->decimal('take_profit_1', 19, 8)->nullable();
            $table->decimal('stop_loss_1', 19, 8)->nullable();
            $table->unsignedInteger('signal_result')->nullable();
            $table->unsignedSmallInteger('status')->default(0);
            $table->string('statement_batch', 10)->nullable()->collation('utf8mb4_unicode_ci');
            $table->timestamp('closed_on')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
