<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEraserTakensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eraser_takens', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->unsignedSmallInteger('buyer_id')->nullable();
            $table->unsignedInteger('amount')->nullable();
            $table->foreign('buyer_id')->references('id')->on('buyers');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eraser_takens');
    }
}
