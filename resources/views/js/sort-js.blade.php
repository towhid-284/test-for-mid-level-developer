<!DOCTYPE html>
<html>
<head>
<title>
JavaScript Array Sort
</title>
<style>
#show{
    font-size:20px;
    color:green;
}
h4{
    margin: 0px;
}
</style>
</head>
<body>

<h4>SORT JS</h4>
<button onclick="myFunction()" class="btn">SORT</button>

<span id="show"></span>

<script>
var cars = ["Bullet", "Opel", "Audi", "Maruti"];
document.getElementById("show").innerHTML = cars;

function myFunction() {
  cars.sort();
  document.getElementById("show").innerHTML = cars;
}
</script>

</body>
</html>
