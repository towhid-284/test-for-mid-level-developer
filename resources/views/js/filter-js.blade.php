<!DOCTYPE html>
<html>
<head>
<title>
JavaScript Filter js
</title>
<style>
#show{
    font-size:20px;
    color:green;
}
h4{
    margin: 0px;
}
</style>
</head>
<body>

<h4>FILTER JS</h4>
<button onclick="myFunction()" class="btn">FILTER</button>

<p id="show"></p>

<script>
var ages = [ 15, 23, 28, 35, 45, 55];
document.getElementById("show").innerHTML = ages;

function checkAdult(age) {
  return age >= 30;
}

function myFunction() {
  document.getElementById("show").innerHTML = ages.filter(checkAdult);
}
</script>

</body>
</html>
