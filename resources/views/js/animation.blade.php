<!DOCTYPE html>
<html>
<head>
<title>
JavaScript Animation js
</title>
<style>
#container {
  width: 200px;
  height: 200px;
  position: relative;
  background: lightgreen;
}
#animate {
  width: 50px;
  height: 50px;
  position: absolute;
  background-color: green;
}
#show{
    font-size:20px;
    color:green;
}
h4{
    margin: 0px;
}
</style>
</head>
<body>

<h4>Animation JS</h4>
<p><button onclick="myMove()" class="btn">Animation</button></p>

<div id ="container">
  <div id ="animate"></div>
</div>

<script>
function myMove() {
  var elem = document.getElementById("animate");
  var pos = 0;
  var id = setInterval(frame, 10);
  function frame() {
    if (pos == 150) {
      clearInterval(id);
    } else {
      pos++;
      elem.style.top = pos + "px";
      elem.style.left = pos + "px";
    }
  }
}
</script>

</body>
</html>
