<!DOCTYPE html>
<html>
<head>
<title>
JavaScript Reduce js
</title>
<style>
#show,#show1{
    font-size:20px;
    color:green;
}
h4{
    margin: 0px;
}
</style>
</head>
<body>

<h4>Reduce JS</h4>
<p id="show1"></p>
<p id="show"></p>

<script>
var numbers = [175, 50, 25];
document.getElementById("show1").innerHTML = numbers;

document.getElementById("show").innerHTML = numbers.reduce(myFunc);

function myFunc(total, num) {
  return total - num;
}
</script>

</body>
</html>
