<!DOCTYPE html>
<html>
<head>
<title>
JavaScript Foreach Sort
</title>
<style>
#show{
    font-size:20px;
    color:green;
}
h4{
    margin: 0px;
}
</style>
</head>
<body>

<h4>FOREACH JS</h4>

<p id="show"></p>

<script>
var cars = ["Bullet", "Opel", "Audi", "Maruti"];
cars . sort();
cars.forEach(myFunction);

function myFunction(item, index) {
  document.getElementById("show").innerHTML += index + " => " + item + "<br>";
}
</script>

</body>
</html>
