<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('sort-js', 'JsController@sort_js');
Route::get('foreach-js', 'JsController@foreach_js');
Route::get('filter-js', 'JsController@filter_js');
Route::get('map-js', 'JsController@map_js');
Route::get('reduce-js', 'JsController@reduce_js');
Route::get('animation', 'JsController@animation');
Route::get('define-callback-js', 'JsController@define_callback_js');
Route::get('i-m-funny', 'QuertionController@i_m_funny');
Route::get('purchase-list-eloquent', 'QuertionController@purchase_list_eloquent');
