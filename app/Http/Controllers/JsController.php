<?php

namespace App\Http\Controllers;

class JsController extends Controller
{
    public function sort_js()
    {
        return view('js.sort-js');
    }
    public function foreach_js()
    {
        return view('js.foreach-js');
    }
    public function filter_js()
    {
        return view('js.filter-js');
    }
    public function map_js()
    {
        return view('js.map-js');
    }
    public function reduce_js()
    {
        return view('js.reduce-js');
    }
    public function animation()
    {
        return view('js.animation');
    }
    public function define_callback_js()
    {
        return view('js.define-callback-js');
    }
}
